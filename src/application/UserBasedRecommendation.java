package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.core.Instance;


public class UserBasedRecommendation {

	public int myUser;//L'utilisateur concerné
	
	double []  CorrCoefficient; // tableau coefficient correlation

	public double[][] matriceNotes;//Matrice qui contient les notes des utilisateurs sur les films
	public Connection con;//Connexion à la BDD
	
	int nbUsers;
	int nbMovies;
	
	ArrayList<Integer> similarUsers;
	int[] nbCommonMovies;
	
	ArrayList<Integer> filmRecommendees;
	ArrayList<Double> noteRecommendees;
	
	
	public UserBasedRecommendation(int user, Connection con){
		this.myUser = user;
		this.con = con;
			
		//Génération des films à recommender:
		//this.getMatrice();
		this.nbUsers = this.getNbUsers();
		this.nbMovies = this.getNbMovies();
		
		this.getMatrice();
		this.getCommonUsers();
		
		//Calculer les coef de corrélation:
		this.CorrCoefficient = new double[this.nbUsers];
		for(int i=0; i<nbUsers; i++){
			this.CorrCoefficient[i] = 0;
		}
		this.CalculateCorrelationCoefficientVector();

		System.out.println("THE END in user based.");
	}
	
	//Récupérer les utilisateurs qui on des films en commun avec myUser:
	public void getCommonUsers(){
		this.nbCommonMovies = new int[this.nbUsers];
		for(int i =0; i<this.nbUsers; i++){
			nbCommonMovies[i] = 0;
			for(int j=0; j<this.nbMovies; j++){
				if(matriceNotes[j][i] != -1 && matriceNotes[j][myUser] != -1){
					nbCommonMovies[i] ++;
				}
			}
		}
		
		//Trouver les 10 premiers qui ont le plus grand nombre de films en commun avec user: 
		int limit = 20;
		this.similarUsers = new ArrayList<Integer>();
		
		for(int i=0; i<limit; i++){
			int indiceMax  = getMax(nbCommonMovies);
			this.similarUsers.add(indiceMax);
		//	System.out.println(""+indiceMax+" Added to list.");
		}
	}
	
	//Récupérer le nombre max de films en commun avec myUser:
	public int getMax(int[] tab){
		int max = -99;
		int indiceMax = 0;
		for(int i=0; i<tab.length; i++){
			if(tab[i] > max){
				max = tab[i];
				indiceMax = i;
			}
		}
		tab[indiceMax] = -99;
		return indiceMax;
	}
	
	/* Méthode qui va créer la matrice (User x Film) qui contient les notes des users sur les films */
	/* Cette matrice va être utilisée pour calculer le coef de corrélation enter myUser et les autres */
	public void getMatrice(){	
		//Instancier la matrice:
		this.matriceNotes = new double[nbMovies][nbUsers];
		
		Statement statement = null;
		try {
			statement = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		int cptNote = 0;

		//Initialiser toute la matrice:
		for(int i=0; i<nbMovies; i++){
			for(int j=0; j<nbUsers; j++){
				matriceNotes[i][j] = -1;
			}
		}
		
		//Récupérer ratings:
		String query = "SELECT userID, itemID, rating FROM ratings;";
		
		//Mettre ratings dans la matrice:
		try {
			ResultSet resultRating = statement.executeQuery(query);	
			while(resultRating.next()){
				Rating rating = new Rating(resultRating.getInt(1), resultRating.getInt(2), resultRating.getInt(3));
				matriceNotes[rating.getItemID()-1][rating.getUserID()-1] = (double)(rating.getRating());
				cptNote ++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Nombre totale des notes: "+cptNote);		
	}
	
	//Récupérer le nombre de fimms depuis la BDD:
	private int getNbMovies(){
		int nbMovies = -1;
		try {
			Statement statement = con.createStatement();
			String query = "Select count(*) from movies;";
			ResultSet result = statement.executeQuery(query);
			while(result.next()){
				nbMovies = result.getInt(1);
				System.out.println("Le nombre de films dans la BDD est: "+nbMovies);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nbMovies;
	}
	
	//Récupérer le nombre des utilisateurs depuis la base de données:
	private int getNbUsers(){
		int nbUsers = -1;
		try {
			Statement statement = con.createStatement();
			String query = "Select count(*) from users;";
			ResultSet result = statement.executeQuery(query);
			while(result.next()){
				nbUsers = result.getInt(1);
				System.out.println("Le nombre de users dans la BDD est: "+nbUsers);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nbUsers;
	}
	
	
	//Calculer la note (Prédiction) pour le film myMovie:
	public double prediction(int myMovie){
		double note = 0;
		double count = 0;
		for(int i=0; i<this.nbUsers; i++){
			if(i != myUser){ //Sauter notre case...
				//on saute l'utilisateur i s'il n'a pas noté le film myMovie
				if(matriceNotes[myMovie][i] != -1){;
					//Si coefCorr = -99, les utilisateurs n'ont pas beaucoup de films en commun, on suate:
					if(CorrCoefficient[i] != -99){
						//CoefCorr de myUser avec i * note de l'utilisateur i pour le film movie
						note = note + (CorrCoefficient[i] * matriceNotes[myMovie][i]);

						count ++;
					}
				}
			}
		}
		if(count != 0) {
			note = note / count;
		}
		if(note != 0){
			//System.out.println("Somme de la note: "+note+" Count: "+count+" Pour le film: "+myMovie);
		}

		return note;		
	}
	
	//Methode pour trouver les films à recommender:
	public void trouverRecommendations(){
		double note;
		this.filmRecommendees = new ArrayList<Integer>();
		this.noteRecommendees = new ArrayList<Double>();
		
		for(int i=0; i<this.nbMovies; i++){
			note = prediction(i);
		//	System.out.println("Film: "+i+" note Predite: "+note);
			System.out.println("Movie: "+i+" predicted rating: "+note);

			if(note > 2.5){
				filmRecommendees.add(i);
				noteRecommendees.add(note);
			}

		}
	}
	
	/* Methode pour récupérer une colonne (notes d'un user) depuis la matrice: */
	public  double [] getColumn(int columnOfInterest){
		double [] colArray =new double[nbMovies];
		for(int row = 0; row < nbMovies; row++)
		{
		    colArray[row] = matriceNotes[row][columnOfInterest];
		}
		
		return colArray;
	}
	// Methode pour eliminer les lignes de -1 entre deux colonnes (movie not rated)
	public double[][] removeNotRated (double[] instance1,double [] instance2){

		int i=0;
		ArrayList<Double> list1 = new ArrayList<Double>();
		ArrayList<Double> list2 = new ArrayList<Double>();
		
		while (i< instance1.length ){
			if (instance1[i]==-1 || instance2[i]==-1){
				//System.out.println("here"+i);
			i++;
			}else{
				list1.add(instance1[i]);
				list2.add(instance2[i]);
				i++;
			}
	     }
	    
		double [][] result = new double [2][list1.size()];

		instance1 = new double[list1.size()];
	    for (int k=0; k < instance1.length; k++)
	    {
	    	instance1[k] = (double)list1.get(k).intValue();
			result[0][k]=instance1[k];

	    }

	    
		instance2 = new double[list2.size()];
		
	    for (int k=0; k < instance2.length; k++)
	    {
	    	instance2[k] = (double)list2.get(k).intValue();
		//	System.out.print("_"+instance2[k]);
			result[1][k]=instance2[k];
	    }
		return result;
	}
	

	public void CalculateCorrelationCoefficientVector(){
		
		
		 double[] instance11= getColumn(myUser);
     	 PearsonCorrelationCoefficient coefcorr= new PearsonCorrelationCoefficient();
     	 
     	 Instance instancex;
     	 Instance instancey;

     	 int cpt = 0;
     	// System.out.println("Coefficient de corrélation: ");
     	 
		 for (int i=0; i<nbUsers;i++){
			 	double[] instance1= getColumn(myUser);
			 	double[] instance2= getColumn(i);
			
				double [][] result=  this.removeNotRated(instance1, instance2);
				instance1 = result [0];
				instance2 = result [1];
			
			  // creation des instances
	     	   instancex = new DenseInstance(instance1);
			   instancey = new DenseInstance(instance2);
			   
			  // calcul de coef coorr
			   if(instance1.length < 10 || !this.isSimilar(i) ){
				   cpt ++;
				   CorrCoefficient[i] = -99;
				   
			   } else{
				   CorrCoefficient[i]=coefcorr.measure(instancex, instancey);
				//   System.out.print(""+CorrCoefficient[i]+" - ");
			   }
		 }	
		 System.out.println("");		
	}
	
	//Vérifier si un utilisateur est "proche" de myUser:
	//C'est à dire s'il est sur la arrayList: similarUsers
	public boolean isSimilar(int i){
		for(int j=0; j<this.similarUsers.size(); j++){
			if(i == this.similarUsers.get(j)) return true;
		}
		return false;
	}
	
}