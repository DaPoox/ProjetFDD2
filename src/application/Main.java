package application;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class Main extends Application {


	public Connection connection;
    private TableView<User> table = new TableView<User>();
    private final ObservableList<User> data = FXCollections.observableArrayList();
    
    public static final int ALLDATA = 0;
    public static final int SELECTDATA = 1;
    public static final int USER_BASED = 2;

    public int choixData = USER_BASED;

    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
    	getListUserFromDataBase();   
        
    	Scene scene = new Scene(new Group());
        stage.setTitle("Movies recommendation system");
        stage.setWidth(800);
        stage.setHeight(500);
        stage.setResizable(false);
        

        final Label label = new Label("List Utilisateurs ");
        label.setFont(new Font("Arial", 20));

        table.setEditable(false);

        TableColumn userId = new TableColumn("User ID");
        userId.setMinWidth(100);
        userId.setCellValueFactory(
                new PropertyValueFactory<User, String>("userID"));

        TableColumn Age = new TableColumn("Age");
        Age.setMinWidth(100);
        Age.setCellValueFactory(
                new PropertyValueFactory<User, String>("age"));

        TableColumn Gender = new TableColumn("Gender");
        Gender.setMinWidth(200);
        Gender.setCellValueFactory(
                new PropertyValueFactory<User, String>("gender"));


        TableColumn Ocupation = new TableColumn("Occupation");
        Ocupation.setMinWidth(200);
        Ocupation.setCellValueFactory(
                new PropertyValueFactory<User, String>("occupation"));

        table.setItems(data);
        table.getColumns().addAll(userId, Age,Gender,Ocupation);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table);
        
        Button buttonOne = new Button("Historique");
        buttonOne.setMaxWidth(Double.MAX_VALUE);
        addHandlerButtonOne(buttonOne, table);
        
        Button buttonTwo = new Button("Recomendation");
        buttonTwo.setMaxWidth(Double.MAX_VALUE);
        addHandlerButtonTwo(buttonTwo);
        
        Button buttonThree = new Button("Evaluation");
        buttonThree.setMaxWidth(Double.MAX_VALUE);
        addHandlerButtonThree(buttonThree);
        
/* Ajout de radiobutton pour choisir les données on  which we're gonna work. */
        VBox vb = new VBox();
        vb.setPadding(new Insets(40, 20, 100, 20));
        vb.setSpacing(10);
        vb.getChildren().addAll(buttonOne, buttonTwo, buttonThree);
        VBox  chBox = new VBox();
        
        ToggleGroup group = new ToggleGroup();
        RadioButton buttonUserBased = new RadioButton("User based");
        buttonUserBased.setToggleGroup(group);
        buttonUserBased.setSelected(true);
        buttonUserBased.selectedProperty().addListener(new ChangeListener<Boolean>(){
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				// TODO Auto-generated method stub
				if(newValue == true)
					choixData = USER_BASED;
			}
        	
        });
        RadioButton button1 = new RadioButton("All database");
        button1.setToggleGroup(group);
        button1.setSelected(false);
        button1.selectedProperty().addListener(new ChangeListener<Boolean>(){

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				// TODO Auto-generated method stub
				if(newValue == true)
					choixData = ALLDATA;
			}
        	
        });
        RadioButton button2 = new RadioButton("Select movies");
        button2.setToggleGroup(group);
        button2.selectedProperty().addListener(new ChangeListener<Boolean>(){

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				// TODO Auto-generated method stub
				if(newValue == true)
					choixData = SELECTDATA;
			}
        	
        });
        chBox.getChildren().addAll(buttonUserBased, button1, button2);
        chBox.setSpacing(10);
        vb.getChildren().add(chBox);
        HBox boxAll = new HBox();
  // ========================================================== 
        boxAll.getChildren().addAll(vbox, vb);
        ((Group) scene.getRoot()).getChildren().addAll(boxAll);
             
        stage.setScene(scene);
        stage.show(); 
    }
    
    //Handler pour le bouton de l'historique (BoutonOne)
    private void addHandlerButtonOne(Button buttonOne, TableView<User> table){
    	EventHandler <ActionEvent> eventButtonOne = new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				//Récupérer l'id de l'utilisateur séléctionné:
				User selectedUser = table.getSelectionModel().getSelectedItem();
				if(selectedUser == null){
					//Afficher un message d'erreur:
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Message du programme");
					alert.setHeaderText("Erreur");
					alert.setContentText("Aucun utilisateur n'a été séléctionné!");

					alert.showAndWait();
				}else{
					int idSelected = selectedUser.getUserID();
					
					//Lancer l'interface pour afficher son historique:
					UserWindow userWindow = new UserWindow(idSelected);
					/* Tout le reste se passe dans le constructeur de la fenêtre. */
				}
			}
    	};
    	//Ajouter le handler au bouton:
    	buttonOne.setOnAction(eventButtonOne);
    }
    
    //Handler pour le bouton des recommendations: ButtonTwo:
    private void addHandlerButtonTwo(Button buttonTwo){
    	EventHandler <ActionEvent> eventButtonTwo = new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				//Récupérer l'id de l'utilisateur séléctionné:
				User selectedUser = table.getSelectionModel().getSelectedItem();
				if(selectedUser == null){
					//Afficher un message d'erreur:
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Message du programme");
					alert.setHeaderText("Erreur");
					alert.setContentText("Aucun utilisateur n'a été séléctionné!");

					alert.showAndWait();
				}else{
					int idSelected = selectedUser.getUserID();
					
					//Lancer l'interface pour afficher les films recommendées:
					RecommendationWindow recommendationWindow = new RecommendationWindow(idSelected, connection, choixData);
					/* Tout le reste se passe dans le constructeur de la fenêtre. */
				}
			}
    	};
    	//Ajouter le handler au bouton:
    	buttonTwo.setOnAction(eventButtonTwo);
    }
    
    //Handler pour le bouton Statistiques: ButtonThree:
    private void addHandlerButtonThree(Button buttonThree){
    	EventHandler <ActionEvent> eventButtonThree = new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				//Lancer la fenêtre Evaluation:
				EvaluationWindow evaluationWindow = new EvaluationWindow(choixData);
			}
    	};
    	//Ajouter le handler au bouton:
    	buttonThree.setOnAction(eventButtonThree);
    }

	public void getListUserFromDataBase(){
    	String url = "jdbc:mysql://localhost:3306/datamovies";
	  	String username = "projetfdd";
	  	String password = "";
	  	Statement statement = null;
	  	ResultSet result = null;

	  	System.out.println("Connecting database...");

	  	try {
	  		connection = DriverManager.getConnection(url, username, password);
	  		System.out.println("Database connected!");	      
	  		statement = (Statement) connection.createStatement();
	  		result = ((java.sql.Statement) statement).executeQuery("SELECT * FROM users;");
	  		int iduser;
	  		char gender;
	  		int age ;
	  		String Ocupation;
	  		while(result.next()){
	  			iduser = Integer.parseInt(result.getString("userID"));
	  			 gender = result.getString("gender").charAt(0);
	  			 age = Integer.parseInt(result.getString("Age"));
	  			 Ocupation=result.getString("occupation");
	  			 data.add(new User(iduser,age,gender,Ocupation));
	  		}
	  		//connection.close();
	  	} catch (SQLException e) {
	      throw new IllegalStateException("Can not connect the database!", e);
	  	}	
	}
}