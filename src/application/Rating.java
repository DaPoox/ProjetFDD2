package application;

public class Rating {
	
	private int userID;
	private int itemID;
	private int rating;
	
	public Rating(int user, int item, int rating){
		this.userID = user;
		this.itemID = item;
		this.rating = rating;
	}

	//Getters & Setter
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getItemID() {
		return itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
}