package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/*
 * L'interface qui concerne un utilisateur:
 * 	- Afficher les films qu'il a noté
 *  * 
 * 	TO DO:
 * 		- ...
 */

public class RecommendationWindow extends Stage{
	
	  int idUser;
	
	  ObservableList<ItemTable> recommendedMovies;
		
      TableView<ItemTable> table = new TableView<ItemTable>();
      
      ArrayList<Integer> idMovies;
      ArrayList<Double> notes;
      Connection con;
      
      
      UserBasedRecommendation recommender;
      
	public RecommendationWindow(int idUser, Connection con, int choixData){
		this.idUser = idUser;
		recommendedMovies = FXCollections.observableArrayList();
		
		this.setTitle("Recommendations pour l'utilisateur: "+this.idUser);
		
		this.con = con;
		
		if(choixData == Main.USER_BASED){
			//UserBased:
			this.recommender = new UserBasedRecommendation(idUser, con);
			this.recommender.trouverRecommendations();
		}else{
			//ContentBased/ContentUserBased
			this.recommender = new ContentUserBasedRecommendation(idUser,  con, choixData);
			((ContentUserBasedRecommendation) this.recommender).getRecommendedMovies();
		}
		Scene scene = creerInterface();
		this.setTitle("User: "+idUser);
		this.setWidth(600);
		this.setHeight(700);
		this.setScene(scene);
		this.show();
	}
	
	/* On les id des films, on a les notes, on va retrouver les titres des films et mettre le tout dans une 
	 * liste de ItemTable:
	 */
	public void setTableElements(){
		this.idMovies = recommender.filmRecommendees;
		this.notes = recommender.noteRecommendees;
		
		Statement statement;
		try {
			statement = con.createStatement();
			int cpt = 0;
			for(int idMovie : this.idMovies){
				//Récupérer le film:
				ResultSet result = statement.executeQuery("SELECT itemID, title FROM movies where itemID = "+idMovie+";");
				result.next();
				ItemTable item = new ItemTable(result.getInt(1), result.getString(2), this.notes.get(cpt));
				this.recommendedMovies.add(item);
				System.out.println("Item: "+item.movieTitle+" rating: "+item.predictedRating);
				cpt ++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//Création de l'interface: 
	public Scene creerInterface(){
		Scene scene = new Scene(new Group());
		
		table.setEditable(false);
		table.setMinHeight(600);
		table.setMinWidth(500);
		TableColumn idFilm = new TableColumn("Id");
		idFilm.setCellValueFactory(new PropertyValueFactory<ItemTable, String>("movieId"));
		idFilm.setMinWidth(30);
		
		TableColumn nomFilm = new TableColumn("Titre");
		nomFilm.setCellValueFactory(new PropertyValueFactory<ItemTable, String>("movieTitle"));
		nomFilm.setMinWidth(300);
	
		TableColumn predictionNote = new TableColumn("Prédiction note");
		predictionNote.setCellValueFactory(new PropertyValueFactory<ItemTable, String>("predictedRating"));	
		predictionNote.setMinWidth(200);		
	
		//Remplire la table:
		this.setTableElements();
		System.out.println("Taille table: "+this.recommendedMovies.size());
		table.setItems(this.recommendedMovies);

		table.getColumns().addAll(idFilm, nomFilm, predictionNote);
		Label label = new Label("Recommendations pour l'utilisateur: "+this.idUser);
		
		VBox vbox = new VBox();
		vbox.setSpacing(5);;
		vbox.setPadding(new Insets(10, 0, 0, 10));;
		vbox.getChildren().addAll(label, table);
		
		((Group)scene.getRoot()).getChildren().addAll(vbox);
		return scene;
	}
	
	public class ItemTable{

		private int movieId;
		private String movieTitle;
		private double predictedRating;
		
		public ItemTable(int id, String movieTitle, double rating){
			this.movieId = id;
			this.movieTitle = movieTitle;
			this.predictedRating = rating;
		}
		
		public int getMovieId(){
			return this.movieId;
		}
		public String getMovieTitle(){
			return this.movieTitle;
		}
		public double getPredictedRating(){
			return this.predictedRating;
		}
		
	}	

}
