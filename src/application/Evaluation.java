package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;


public class Evaluation {

	static int sizetab=3;
	static int IDItem[]={91,479, 444};
	static List<Integer> IDUser= new ArrayList<>();
	public static int[][] matriceNotesBase;
	public static double[][] matriceNotesPredites;
	static Statement statement = null;
	static ResultSet result = null;
	static String url = "jdbc:mysql://localhost:3306/datamovies";
	static String username = "projetfdd";
	static String password = "";
	static int ResultatsComparaisons[];
	static int nombreDe1=0;
	static int nombreDe0=0;

	static ArrayList<UserBasedRecommendation> userBasedRecommendationList;
	static ArrayList<ContentUserBasedRecommendation> contentUserBasedRecommendationList;

	static Connection con;

	static int choixData;
	public Evaluation(int choix){
		choix = choixData;
		
		GetUseRating(IDItem,SelectListUser(IDItem)); 
		setRecommendationList();//Créer les objets "Recommendation"
		GetMatricePrediction();
		Comparaison(matriceNotesPredites,matriceNotesBase);
	}


	//Création des objets Recommendation pour les utiliser après pour faire la prédictions:
	public static void setRecommendationList(){
		if(choixData == Main.USER_BASED){
			userBasedRecommendationList = new ArrayList<UserBasedRecommendation>();
			contentUserBasedRecommendationList = null;
			for(int i=0; i<IDUser.size(); i++){
				UserBasedRecommendation recommendationUser = new UserBasedRecommendation(IDUser.get(i), con);
				userBasedRecommendationList.add(recommendationUser);
			}
			System.out.println("Nb users: "+IDUser.size()+" nbRecommendations: "+userBasedRecommendationList.size());
		}else{
			contentUserBasedRecommendationList = new ArrayList<ContentUserBasedRecommendation>();
			userBasedRecommendationList = null;
			for(int i=0; i<IDUser.size(); i++){
				ContentUserBasedRecommendation recommendationContent = new ContentUserBasedRecommendation(IDUser.get(i), con, choixData);
				contentUserBasedRecommendationList.add(recommendationContent);
			}
			System.out.println("Nb users: "+IDUser.size()+" nbRecommendationsContent: "+contentUserBasedRecommendationList.size());

		}
	}
	/*LA methode pour récuperer la liste des utilisateurs ayant vus les films donnés en entrée*/
	public static List<Integer> SelectListUser(int []tab){


		System.out.println("Connecting database...");

		try {
			con = DriverManager.getConnection(url, username, password);
			System.out.println("Database connected!");	      
			statement = (Statement) con.createStatement();


			String chaine="";
			for(int i=0; i<sizetab-2;i++){
				chaine=chaine+tab[i]+",";
			}
			chaine=chaine+tab[sizetab-1];

			//Génération de la requete:
			String requete = "";
			for(int i=0; i<IDItem.length-1; i++){
				requete += "SELECT userID FROM ratings WHERE itemID = "+IDItem[i]+" AND userID IN (";
			}
			requete += "SELECT userID FROM ratings WHERE itemID = "+IDItem[IDItem.length-1];
			for(int i=0; i<IDItem.length-1; i++){
				requete += ")";
			}
			System.out.println("Requete: "+requete);


			//result = ((java.sql.Statement) statement).executeQuery("SELECT DISTINCT userID FROM ratings WHERE itemID IN ("+chaine+");");
			//Exécution de la requete:
			result = statement.executeQuery(requete);
			int iduser = 0;
			while(result.next()){
				//Récupération de l'utilisateur
				iduser = Integer.parseInt(result.getString("userID"));	
				IDUser.add(iduser);

				System.out.println(iduser);

			}
			
		} catch (SQLException e) {
			e.printStackTrace();

			//	      throw new IllegalStateException("Can not connect the database!", e);
		}
		System.out.println(" taille: "+IDUser.size());
		return IDUser;

	}

	/*La methode pour remplir la matrice des notes des utilisateurs recupérés precedement */
	public static int[][] GetUseRating(int []listItem,List<Integer> listUser){
		matriceNotesBase= new int [IDUser.size()][sizetab];
		try (Connection connection = DriverManager.getConnection(url, username, password)) {
			System.out.println("Database connected!");	      
			statement = (Statement) connection.createStatement();

			for(int i=0; i<IDUser.size();i++){
				for(int j=0; j<sizetab;j++){
					result = ((java.sql.Statement) statement).executeQuery("SELECT rating FROM ratings WHERE userID="+IDUser.get(i)+" AND  itemID="+listItem[j]+" ;");

					if(result.next()){
						matriceNotesBase[i][j]= result.getInt(1);	
					}else matriceNotesBase[i][j] = -99;
				}
			} 


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*affichage de la matrice Notes utilisateurs*/
		System.out.println("**************************Matrice***************************");

		for(int i=0; i<IDUser.size();i++){
			for(int j=0; j<sizetab;j++){
				System.out.print(matriceNotesBase[i][j]+" - ");
			}
			System.out.println("");
		}
		return matriceNotesBase;
	}

	/*remplir matrice des resulatats prédits: */

	public static double [][] GetMatricePrediction(){
		/******Matrice des notes predites *******/
		System.out.println("*****************matrice des notes prédites***********************");
		matriceNotesPredites= new double [IDUser.size()][sizetab];
		for(int i=0; i<IDUser.size();i++){
			for(int j=0; j<sizetab;j++){
				//			matriceNotesPredites[i][j]=(int) (Math.random() * 5+ 1);
				if(choixData == Main.USER_BASED){
					matriceNotesPredites[i][j] = userBasedRecommendationList.get(i).prediction(IDItem[j]);
				}else{
					matriceNotesPredites[i][j] = contentUserBasedRecommendationList.get(i).prediction(IDItem[j]);
				}
			}
		}
		for(int i=0; i<IDUser.size();i++){
			for(int j=0; j<sizetab;j++){
				System.out.print(matriceNotesPredites[i][j]+" - ");
			}
			System.out.println("");
		}
		return matriceNotesPredites;
	}

	/* La methode comparaison*/
	public static int[] Comparaison(double [][]matriceA,int[][]matriceB){
		ResultatsComparaisons= new int[IDItem.length*IDUser.size()];
		int indice=0;
		for(int i=0; i<IDUser.size();i++){
			for(int j=0; j<sizetab;j++){
				if((matriceA[i][j]>2.5 && matriceB[i][j]>2.5)||
						(matriceA[i][j]<2.5 && matriceB[i][j]<2.5)){
					ResultatsComparaisons[indice]=1;
					indice++;
				}
				else{
					ResultatsComparaisons[indice]=0;
					indice++;
				}
			}
		}

		return ResultatsComparaisons;
	}

	/*Interpréter les resultats*/

	public static int InterprerBonnePrediction(){
		for(int i=0; i<ResultatsComparaisons.length;i++){
			if(ResultatsComparaisons[i]==1){
				nombreDe1++;
			}
		}
		return nombreDe1;
	}

	public static int InterprerPredictionErronne(){
		for(int i=0; i<ResultatsComparaisons.length;i++){
			if(ResultatsComparaisons[i]==0){
				nombreDe0++;
			}
		}
		return nombreDe0;
	}

}