package application;

public class User {
	
		private int userID;
		private int age;
		private char gender;
		private String occupation;
		
		public User(int id, int age, char gender, String occupation){
			this.userID = id;
			this.age = age;
			this.gender = gender;
			this.occupation = occupation;
		}
		
		//Getters & Setter:
		public int getUserID() {
			return userID;
		}
		public void setUserID(int userID) {
			this.userID = userID;
		}
		
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		
		public char getGender() {
			return gender;
		}
		public void setGender(char gender) {
			this.gender = gender;
		}
		
		public String getOccupation() {
			return occupation;
		}
		public void setOccupation(String occupation) {
			this.occupation = occupation;
		}
		
		public String toString(){
			return ("Utiliateur: "+this.userID+", age: "+this.age+", sexe:"+this.gender+", Occupation: "+this.occupation);
		}
}