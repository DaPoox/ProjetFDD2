package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/*
 * L'interface qui concerne un utilisateur:
 * 	- Afficher les films qu'il a noté
 *  * 
 * 	TO DO:
 * 		- ...
 */

public class UserWindow extends Stage{
	
	  int idUser;
	
	  ObservableList<Movies> historique;
	
	  Connection con;
	
      TableView<Movies> table = new TableView<Movies>();

	public UserWindow(int idUser){
		this.idUser = idUser;
		historique = FXCollections.observableArrayList();
		this.setTitle("Historique de l'utilisateur: "+this.idUser);
		
		//Se connecter à la base de données:
		con = connect(idUser);
		generateHistorique(con, idUser);
		
		Scene scene = creerInterface();
		this.setTitle("User: "+idUser);
		this.setWidth(500);
		this.setHeight(800);
		this.setScene(scene);
		this.show();
	}
	
	//Création de l'interface: 
	public Scene creerInterface(){
		Scene scene = new Scene(new Group());
		
		table.setEditable(false);
		table.setMinHeight(700);
		TableColumn idFilm = new TableColumn("Id");
		idFilm.setCellValueFactory(new PropertyValueFactory<Movies, String>("movieId"));
		idFilm.setMinWidth(30);
		
		TableColumn nomFilm = new TableColumn("Titre");
		nomFilm.setCellValueFactory(new PropertyValueFactory<Movies, String>("movieTitle"));
		nomFilm.setMinWidth(200);
		
		TableColumn dateFilm = new TableColumn("Date de sortie");
		dateFilm.setCellValueFactory(new PropertyValueFactory<Movies, String>("releaseDate"));	
		dateFilm.setMinWidth(100);
		
		
		table.setItems(historique);

		table.getColumns().addAll(idFilm, nomFilm, dateFilm);
		
		Label label = new Label("Historique de l'utilisateur: ");
		
		VBox vbox = new VBox();
		vbox.setSpacing(5);;
		vbox.setPadding(new Insets(10, 0, 0, 10));;
		vbox.getChildren().addAll(label, table);
		
		((Group)scene.getRoot()).getChildren().addAll(vbox);
		return scene;
	}
	//Connexion avec la base de données
	private Connection connect(int idUser){

		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/datamovies","projetfdd","");  
			System.out.println("Connected");
			return con;
			
		}catch(Exception e){
			System.out.println("Exception while connecting");
			e.printStackTrace();
			return null;
		}
	}
	
	//Méthode pour récupérer la liste des films (que l'utilisateur a noté) depuis la BDD:
	public void generateHistorique(Connection con, int idUser){
		try{
			Statement stmt=con.createStatement();  
			String requete = "select * from movies where itemID IN (select itemID from ratings where userID = "+idUser+")";
		
			System.out.println("Requete: "+requete);
		
			ResultSet rs=stmt.executeQuery(requete);  
		
			System.out.println("Query executed.");
			while(rs.next()){
				//	System.out.println("itemID: "+rs.getInt(1));
				Movies movie = new Movies(rs.getInt(1), rs.getString(2), rs.getDate(3).toString());
				historique.add(movie);
			}		
		}catch(Exception e){
			System.out.println("Exception while getting data");
			e.printStackTrace();
		}
	}

}
