package application;

public interface Recommendation {
	
		int[] getRecommendation();
}
