package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ContentUserBasedRecommendation extends UserBasedRecommendation {

	public ArrayList<Integer> movieSet;//Ensemble de films avec le-quel on va travailler

	public double[][] moviesGenres;
	int nbGenres = 19;
	
	public int[] users;//Liste des users avec une forte corrélation avec nous
	
	public double[] ratings;//Tableau qui contient les notes de l'utilisateur sur un film. -1 si la note n'existe pas.
	
	
	public double[] idf;
	public double[] df;
	public double[] usersProfile;
	
	public ContentUserBasedRecommendation(int user, Connection con, int choixData){
		super(user, con);
		if(choixData == Main.SELECTDATA){
			//Récupération des users qui me ressembles: 
			//Parcourir le tableau qui contient les coef de corrélation, et prendre les users
			//Qui ont un coef de corrélation plus grand que 0.5
			getCloseUsers();
		}else if(choixData == Main.ALLDATA){
			//Sinon, travailler sur toute la base de données
			//DO NOTHING.
		}
		
		this.getMoviesSet();
		getGenres();
		
		getRatings();
		
		//Clean our matrix: 
		this.cleanData();		
		

		//Calculer le TF-IDF:
		getTF_IDF();
				
		//Calculer le profile de l'utilisateur:
		getUsersProfile();
				
	}
	
	
	//Méthode pour récupérer les users qui ont une forte corrélation avec nous:
	public void getCloseUsers(){
		/*Le coefficient de corrélation est calculé dans le constructeur de la classe mère*/
		int nbSimilarUsers = 0;
			
		// LIMIT: prendre que les users qui ont une corrélation > limitCorrCoef
		double limitCorrCoef = 0.5;
			
		ArrayList <Integer >myUsers = new ArrayList<Integer>();
			
		//Parcourir les coef de corr et ajouter les user qui ont plus que la limit:
		myUsers = new ArrayList<Integer>();
		for(int i=0; i<super.CorrCoefficient.length; i++){
			if(super.CorrCoefficient[i] > limitCorrCoef){
				myUsers.add(i+1);//Les users commence de 1, le tab de coef de corrélation commence de 0...
				nbSimilarUsers ++;
			}
		}
		
		//Mettre les users dans notre tableau de users:
		this.users = new int[nbSimilarUsers];
		System.out.println("Nombre de users qu'on va utiliser: "+nbSimilarUsers);
	//	System.out.println("Les users qu'on va utiliser: ");
		for(int i=0; i<nbSimilarUsers; i++){
		//	System.out.print(""+myUsers.get(i)+" - ");
			this.users[i] = myUsers.get(i);
		}
		System.out.println("");
	}
	//Générer l'ensemble de films!
		public void getMoviesSet(){
			
			//Récupérer leurs films depuis la base de données:
			try {
				Statement statement = this.con.createStatement();
				
				String query = "";
				//Si on travaille sur toute la base de données, on récupère toutes les films
				if(users == null){
					query = "SELECT itemID FROM movies;";
				}
			else{
				//Sinon, on récupère les films selon notre séléction
				query = "SELECT DISTINCT itemID FROM ratings WHERE userID IN (";
				for(int i=0; i<users.length-1; i++) query += users[i]+", ";
				query += users[users.length-1]+");";
				System.out.println(query);
			}	
				ResultSet result = statement.executeQuery(query);
				
				movieSet = new ArrayList<Integer>();
				
				//Ajouter les films sur notre liste (movieSet):
				while(result.next()){
					movieSet.add(result.getInt(1));
				}
						
				//Récupérer les films qui non pas été notés:
				query = "SELECT itemID FROM movies where itemID NOT IN (SELECT itemID FROM ratings);";
				result = statement.executeQuery(query);
				while(result.next()){
					movieSet.add(result.getInt(1));
				}
				
				this.nbMovies = movieSet.size();
						
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void getGenres(){
			//Récupérer les genres des films:
			moviesGenres = new double[movieSet.size()][nbGenres];
			for(int i=0; i<movieSet.size(); i++){
				int idMovie = this.movieSet.get(i);
				String query = "SELECT * FROM movies WHERE itemID = "+idMovie+";";
				ResultSet result;
				try {
					Statement statement = con.createStatement();
					result = statement.executeQuery(query);
					if(result.next()){
						for(int j=0; j<nbGenres; j++){
							moviesGenres[i][j] = result.getInt(j+6);//On saute les première colonnes (on va directement aux genres)
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		//get les notes de l'user myUser sur ces films (-1 s'il n'a pas noté):
		public void getRatings(){
			ratings = new double[movieSet.size()];
			for(int i=0; i<movieSet.size(); i++){
				String query = "SELECT rating FROM ratings WHERE itemID = "+movieSet.get(i)+" AND userID = "+myUser+";";
				try {
					Statement statement = con.createStatement();
					ResultSet result = statement.executeQuery(query);
					if(result.next()){
						ratings[i] = result.getInt(1);
					}else ratings[i] = -1;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
		/* Nettoyer la matrice: Enlever les colonnes inutiles
		 * Ceux qui contiennent que des 0
		 */
		public void cleanData(){
			ArrayList<Integer> useless = new ArrayList<Integer>();
			//Récupérer les clonnes inutiles:
			for(int i=0; i<nbGenres; i++){
				int nb1 = 0;
				for(int j=0; j<nbMovies; j++){
					if(moviesGenres[j][i] == 1){
						nb1 ++;
						break;
					}
				}
				if(nb1 == 0){
					//ajouter le numéro de la colonne à notre liste:
					useless.add(i);
				}
			}
			
			//Supprimer les colonnes inutiles:
			double[][] nouvelleMatrice = new double[nbMovies][nbGenres - useless.size()];
			int cpt = 0;
			for(int i=0; i<nbGenres; i++){
				if(useless.indexOf(i) == -1){
					//La colonne n'éxiste pas dans la liste Useless, on l'ajoute...
					for(int j=0; j<nbMovies; j++){
						nouvelleMatrice[j][cpt] = moviesGenres[j][i];
					}
					cpt ++;
				}
			}
			//Mettre à jour la matrice:
			this.moviesGenres = nouvelleMatrice;
			this.nbGenres  = cpt;
		}
		
		//Methode pour faire la normalisation:
		public void getTF_IDF(){
			//Calculer le idf:
			getDF();
			getIDF();
							
			//Calculer tf-idf:
	//		System.out.println("TF-IDF:");
			for(int i=0; i<nbMovies; i++){
				for(int j=0; j<nbGenres; j++){
					//TF-IDF = TF * IDF;
					moviesGenres[i][j] = moviesGenres[i][j] * idf[j];
				}
			}
/*			System.out.println(" ========== TF-IDF ========== ");
			//Affichage de la matrice:
			for(int i=0; i<nbMovies; i++){
				System.out.print("Movie "+(i+1)+": ");
				for(int j=0; j<nbGenres; j++){
					System.out.print(""+moviesGenres[i][j]+" - ");
				}
				System.out.println("");
			}
	*/		
		}

		//Méthode pour calculer TF pour chaque genre/film:
		public void getTF(){
			//Calculer TF: 
			for(int i=0; i<nbMovies; i++){
				//Calculer le nombre de termes;
				int nbTermes = 0;
				for(int j=0; j<nbGenres; j++){
					if(moviesGenres[i][j] == 1) nbTermes ++;
				}
				//Calculer le tf:
				for(int j=0; j<nbGenres; j++){
					moviesGenres[i][j] = moviesGenres[i][j]/nbTermes;
				}
			}
		}
		//Méthode pour calculer IDF:
		/* Calculer les valeurs de IDF de user pour chaque genres:
		 * En se basant sur la formule donnée au cour:
		 * 	IDF = ln(n/DF) avec DF = nombre de fois ou le genre apparait dans l'ensemble des films
		 */
		public void getIDF(){
			idf = new double[nbGenres];
			//Calculer IDF pour chaque genre:
			for(int i=0; i<nbGenres; i++){
				idf[i] = Math.log(nbMovies/df[i]);
			}
	/*			
			System.out.print("IDF:");
			//Afficher le tableau des IDF :
			for(int i=0; i<nbGenres; i++){
				System.out.print(""+idf[i]+" - ");
			}
			System.out.println("");
	*/	
		}
		//Méthode pour calculer DF:
		public void getDF(){
			double cpt=0;
			df = new double[nbGenres];
			for (int i=0; i<nbGenres;i++){
				cpt=0;
				for (int j=0; j<movieSet.size(); j++){
					if (moviesGenres[j][i]!=0){
						cpt++;
					}
				}
				
				df[i]= cpt;
				cpt=0;
			}
			
			//Affichage: 
	/*		System.out.print("DF: ");
			for(int i=0; i<nbGenres; i++){
				System.out.print(""+df[i]+" - ");
			}
			System.out.println("");
	*/		
		}
		
		//Méthode pour calculer profile utilisateur:
		public void getUsersProfile(){
			//Calcule du profile de l'utilisateur: 
			usersProfile = new double[nbGenres];
			for(int i=0; i<nbGenres; i++){
				usersProfile[i] = 0;
				int cpt = 0;
				for(int j=0; j<movieSet.size(); j++){
					if(ratings[j] != -1){
						//L'utilisateur a aimé ce film
						usersProfile[i] += moviesGenres[j][i]*(ratings[j]);
						cpt ++;
					}
				}
				usersProfile[i] = usersProfile[i] / cpt;
			}
					
			//Affichage:*
		/*	System.out.println("User's profile: ");
			for(int i=0; i<nbGenres; i++){
				System.out.print(""+usersProfile[i]+" - ");
			}	
			System.out.println("");;
		*/
		}
		
		//Prédiction de la note de l'user pour un film donné:
		public double prediction(int movie){
			double note = 0;
			for(int i=0; i<nbGenres; i++){
				note += usersProfile[i]* moviesGenres[movie][i]; 
			}
			double racineTFIDF = 0;
			double racineUserProfile = 0;
			for(int i=0; i<nbGenres; i++){
				racineTFIDF += Math.pow(moviesGenres[movie][i], 2);
				racineUserProfile += Math.pow(usersProfile[i], 2);
			}
			racineTFIDF = Math.sqrt(racineTFIDF);
			racineUserProfile = Math.sqrt(racineUserProfile);
			note = note / (racineTFIDF * racineUserProfile);
			note *= 5;
			return note;
		}
		//Trouver les films à recommender:
		public void getRecommendedMovies(){
			/*Parcourir la liste des films, et si l'utilisateur n'a pas regarder un film, on fait la prédiction de sa note
			* Si la note > .2.5, on recommende le film
			*/
			this.filmRecommendees = new ArrayList<Integer>();
			this.noteRecommendees = new ArrayList<Double>();
			double noteLimit = 3.0;
			for(int i=0; i<nbMovies; i++){
				if(ratings[i] == -1){
					double note = prediction(i);
					System.out.println("film: "+i+" note: "+note);
					//TODO: add movie to list if note >2.5
					if(note>noteLimit){
						this.filmRecommendees.add(i);
						this.noteRecommendees.add(note);
					}
				}
			}
		}
	
}
