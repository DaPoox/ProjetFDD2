package application;

import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class EvaluationWindow extends Stage{
	
	Evaluation evaluation;
	
	public EvaluationWindow(int choixData){
		evaluation = new Evaluation(choixData);
		creerInterface();
	}
	
	//Création de l'interface:
	public void creerInterface(){
		VBox root = new VBox();
		BarChart<String, Number> barChart = addChart();
		root.getChildren().addAll(barChart);
		
		Scene scene = new Scene(root, 600, 400);
		this.setWidth(600);
		this.setHeight(700);
		this.setScene(scene);
		this.show();
	}
	
	//Création du graphe:
	public BarChart<String, Number> addChart(){
		String VRAI = "Prediction\ncorrecte";
		String FAUX = "Fausse\nprediction";
		
		//Création des axes X et Y, et l'objet du graphe:
		CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("Résultat");

		NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Nombre de prédictions");
		
		BarChart<String, Number> chart = new BarChart<String, Number>(xAxis, yAxis);
		chart.setTitle("Evaluation des prédictions faites par l'algorithme");
		
		Series serieFaux = new Series();
		Series serieVrai = new Series();
		serieFaux.setName("Fausses predictions");
		serieVrai.setName("Predictions correctes");
		/*Ajouter les informations: nombre de fois où la prédiction était correte
		 * Et le nombre de fois où l'algorithme a fait de fausses prédictions
		 */
		int nombreCorrectes =  evaluation.InterprerPredictionErronne();
		int nombreFaussePredictions = evaluation.InterprerPredictionErronne();
		serieFaux.getData().add(new Data(FAUX,nombreCorrectes));
		serieVrai.getData().add(new Data(VRAI, nombreFaussePredictions));
		
		//Ajouter les données au graphe:
		chart.getData().addAll(serieFaux, serieVrai);
		chart.setCategoryGap(100);
		chart.setBarGap(-50);
		
		
		//Modifier les propriétés du graphe:
		chart.setBarGap(-10);
		return chart;
	}

}