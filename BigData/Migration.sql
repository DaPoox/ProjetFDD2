DROP TABLE IF EXISTS movies.ratings;
DROP TABLE IF EXISTS movies.users;
DROP TABLE IF EXISTS movies.movies;

CREATE TABLE IF NOT EXISTS datamovies.users (
    userID INT PRIMARY KEY,
    age INT,
    gender TEXT,
    occupation TEXT,
    zipcode TEXT 
    );
LOAD DATA LOCAL INFILE 'u.user' INTO TABLE datamovies.users FIELDS TERMINATED BY '|';


CREATE TABLE IF NOT EXISTS datamovies.movies (
    itemID INT PRIMARY KEY NOT NULL,
    title TEXT,
    releaseDate DATE,
    videoReleaseDate TEXT,
    IMDbURL TEXT,
    unknown TINYINT,
    Action TINYINT,
    Adventure TINYINT,
    Animation TINYINT,
    Children TINYINT,
    Comedy TINYINT,
    Crime TINYINT,
    Documentary TINYINT,
    Drama TINYINT,
    Fantasy TINYINT,
    FilmNoir TINYINT,
    Horror TINYINT,
    Musical TINYINT,
    Mistery TINYINT,
    Romance TINYINT,
    SciFi TINYINT,
    Thriller TINYINT,
    War TINYINT,
    Western TINYINT
    );
LOAD DATA LOCAL INFILE 'u.item' INTO TABLE datamovies.movies FIELDS TERMINATED BY '|'
	(itemID, title, @var3, videoReleaseDate, IMDbURL, unknown, Action, Adventure, Animation, Children, 
	Comedy, Crime, Documentary, Drama, Fantasy, FilmNoir, Horror, Musical, Mistery, Romance, SciFi, Thriller, 
	War, Western)
set releaseDate = STR_TO_DATE(@var3,'%d-%M-%Y');
/*set videoReleaseDate = STR_TO_DATE(@var4, '%d-%M-%Y');*/

CREATE TABLE IF NOT EXISTS datamovies.ratings (
    userID INT ,
    itemID INT ,
    rating INT,
    timestamp INT,
    FOREIGN KEY (userID) REFERENCES users(userID),
    FOREIGN KEY (itemID) REFERENCES movies(itemID)
    );

LOAD DATA LOCAL INFILE 'u.data'
INTO TABLE datamovies.ratings;
